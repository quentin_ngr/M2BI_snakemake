configfile: "config/config.yaml",

rule all:
    input:
	expand("results/trimming/{sample}_trim_paired.fastq.gz", sample = config["samples"]),

rule trimmomatic:
    input:
         expand("{sample}.fastq.gz", sample = config["samples"])
    output:
	expand("results/trimming/{sample}_trim_paired.fastq.gz", sample = config["samples"]),
        expand("results/trimming/{sample}_trim_unpaired.fastq.gz", sample = config["samples"])
    conda:
	"envs/trimmomatic.yml"
    resources:
	mem_mb=4000,
        time="00:30:00"
    shell: "trimmomatic PE -threads 4 \
    -trimlog logs/trim.log  \
    {input} \
    ILLUMINACLIP:"$DATA_DIR"/NexteraPE-PE.fa:2:30:10:2:keepBothReads \
    LEADING:3 \
    TRAILING:3 \
    SLIDINGWINDOW:4:15 \
    MINLEN:33"


