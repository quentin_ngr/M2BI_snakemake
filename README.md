## Snakemake project

The goal of this project is to make a snakemake script in order to analyse the rna_seq data extracted from mouse B cells in the context of cell differenciation due to Ikaros (full article : https://doi.org/10.1038/s41597-019-0202-7).


### Repository description

This repository has 4 main directories : 
-data, in which reads and other types of raw data are stored
-workflow, in which scripts and conda environnement are stored
-config, in which there is a config file for snakemake
-logs, in which logs are written

### Prerequisite

Conda environnement should be installed beforehand.

### Lauching scripts

Lauch the QC then the trimming using this scripts 

snakemake --cores 8 --jobs 100 --use-conda --snakefile _insert files_ --default-resources




Authors : Quentin Nugier (Master 2 bioinformatics in Université Clermont Auvergne)
